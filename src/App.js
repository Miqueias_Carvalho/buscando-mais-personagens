import "./App.css";
import Characters from "./components/Characters";
import { useState, useEffect } from "react";

function App() {
  const [characterList, setCharacterList] = useState([]);
  // const [page, setPage] = useState(1);
  const [next, setNext] = useState(
    "https://rickandmortyapi.com/api/character/"
  );

  useEffect(() => {
    fetch(next)
      .then((response) => response.json())
      .then((response) => {
        setCharacterList([...characterList, ...response.results]);
        setNext(response.info.next);
      })
      .catch((err) => console.log(err));
  }, [next]);
  // console.log(characterList);

  // const previousPage = () => {
  //   if (page > 1) {
  //     setPage(page - 1);
  //   }
  // };

  // const nextpage = () => {
  //   if (page != null) {
  //     setPage(page + 1);
  //   }
  // };

  return (
    <div className="App">
      <header className="App-header">
        <Characters characterList={characterList} />
        {/* <div>
          <button onClick={previousPage}>voltar</button>
          <button onClick={nextpage}>avançar</button>
        </div> */}
      </header>
    </div>
  );
}

export default App;
